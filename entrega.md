# ENTREGA CONVOCATORIA JUNIO
Eva López Sánchez

- Correo electrónico: e.lopezs.2022@alumnos.urjc.es
- Enlace al vídeo: https://youtu.be/dAEoJzTxjko
- Requisitos mínimos:
  - mirror
  - grayscale
  - blur
  - change_colors
  - rotate
  - shift
  - crop
  - filter
  - transform_simple
  - transform_args
  - transform_multi
- Requisitos opcionales:
  - sepia
  - luminity
  - negative